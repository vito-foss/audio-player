/*
 * This file is part of AudioPlayer (https://gitlab.com/vitoyucepi/audio-player)
 * Copyright (C) 2023 Vitoyucepi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import {cssRulesTest, fontRulesTest, tsRulesTest} from '../common';
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-expect-error
import postcssPresetEnv from 'postcss-preset-env';
import {CleanWebpackPlugin} from 'clean-webpack-plugin';
import cssnano from 'cssnano';
import MiniCssExtractPlugin from 'mini-css-extract-plugin';
import webpack, {Configuration, RuleSetCondition, RuleSetRule, RuleSetUseItem, WebpackPluginInstance} from 'webpack';
import TerserPlugin from 'terser-webpack-plugin';
import svgToTinyDataUri from 'mini-svg-data-uri';
import {gzip, ZopfliOptions} from '@gfx/zopfli';
import CompressionPlugin from 'compression-webpack-plugin';
import WebpackUserscript, {HeadersProps, RunAt} from 'webpack-userscript';
import postcssInputRange from 'postcss-input-range';

export interface CssLoadersOptions {
  useFontCdnSource: boolean;
}

export interface CssLoadersResult {
  cssRulesInline: RuleSetRule;
  cssRulesFile: RuleSetRule;
}

export const cssLoaders = ({useFontCdnSource}: CssLoadersOptions): CssLoadersResult => {
  const cssLoaders: RuleSetUseItem[] = [
    {loader: 'css-loader', options: {importLoaders: 1, modules: {auto: true}}},
    {
      loader: 'postcss-loader',
      options: {
        postcssOptions: {
          plugins: [
            postcssInputRange(),
            // eslint-disable-next-line @typescript-eslint/no-unsafe-call
            postcssPresetEnv({stage: 0}),
            cssnano({preset: ['default', {discardComments: {removeAll: true}}]}),
          ],
        },
      },
    },
    {
      loader: 'sass-loader',
      options: {additionalData: `$font-source-cdn: ${useFontCdnSource.toString()};`, api: 'legacy'},
    },
  ];

  const cssRulesInline: RuleSetRule = {
    test: cssRulesTest,
    use: ['style-loader', ...cssLoaders],
  };

  const cssRulesFile: RuleSetRule = {
    test: cssRulesTest,
    use: [MiniCssExtractPlugin.loader, ...cssLoaders],
  };
  return {cssRulesInline, cssRulesFile};
};

const imageRules: RuleSetCondition = /\.(?:png|jpe?g|gif)$/i;
export const imageRulesInline: RuleSetRule = {
  test: imageRules,
  type: 'asset/inline',
  use: ['image-webpack-loader'],
};
export const imageRulesFile: RuleSetRule = {
  test: imageRules,
  type: 'asset/resource',
  use: ['image-webpack-loader'],
};

const svgRules: RuleSetCondition = /\.svg$/i;

// noinspection JSUnusedGlobalSymbols
const svgLoaderGenerator = {dataUrl: (content: string): string => svgToTinyDataUri(content.toString())};
export const svgRulesInline: RuleSetRule = {
  test: svgRules,
  type: 'asset/inline',
  generator: svgLoaderGenerator,
  use: ['image-webpack-loader'],
};

export const svgRulesFile: RuleSetRule = {
  test: svgRules,
  type: 'asset/resource',
  use: ['image-webpack-loader'],
};

export const optimization = (): Configuration['optimization'] => ({
  minimize: true,
  minimizer: [
    new TerserPlugin({
      terserOptions: {
        compress: true,
        output: {
          comments: false,
        },
      },
      extractComments: false,
    }),
  ],
  mangleExports: 'size',
  moduleIds: 'size',
});

export const optimizationSplit: Configuration['optimization'] = {
  mangleExports: 'deterministic',
  moduleIds: 'deterministic',
  runtimeChunk: 'single',
  splitChunks: {
    cacheGroups: {
      vendor: {
        test: /[\\/]node_modules[\\/]/,
        name: 'vendors',
        chunks: 'all',
      },
    },
  },
};

export const fontRulesInline: RuleSetRule = {
  test: fontRulesTest,
  type: 'asset/inline',
};
export const fontRulesFile: RuleSetRule = {
  test: fontRulesTest,
  type: 'asset/resource',
};

export const tsRules: RuleSetRule = {
  test: tsRulesTest,
  use: [{loader: 'ts-loader', options: {compilerOptions: {sourceMap: false}, allowTsInNodeModules: true}}],
};

export const cleanWebpackPlugin = (): WebpackPluginInstance => new CleanWebpackPlugin();
export const miniCssExtractPlugin = (): WebpackPluginInstance =>
  new MiniCssExtractPlugin({filename: '[name].[contenthash].css'});
export const compressionRulesTest: CompressionPlugin.Rule = /\.(?:js|css|html|svg)$/i;
export const compressionPlugins = (): WebpackPluginInstance[] => [
  new CompressionPlugin<ZopfliOptions>({
    filename: '[file].gz[query]',
    test: compressionRulesTest,
    compressionOptions: {
      numiterations: 15,
    },
    algorithm: gzip,
    threshold: 10240,
    minRatio: 0.8,
  }),
  new CompressionPlugin({
    filename: '[file].br[query]',
    algorithm: 'brotliCompress',
    test: compressionRulesTest,
    compressionOptions: {
      level: 11,
    },
    threshold: 10240,
    minRatio: 0.8,
  }),
];

export const webpackDefinePlugin = new webpack.DefinePlugin({
  PRODUCTION: true,
});

export const stats: Configuration['stats'] = {
  preset: 'normal',
  colors: true,
};

interface FormatBaseUrlOptions {
  baseUrl?: string;
  releaseChannel?: string;
  suffix?: string;
}

const defaultBaseUrl = 'https://unpkg.com/@vitoyucepi/audioplayer-userscript';

function getReleaseChannel(): string {
  let releaseChannel = process.env.npm_config_dist_tag || 'latest';
  const version = process.env.npm_package_version || '0.0.0';
  if (releaseChannel === 'experimental') {
    releaseChannel = version;
  }
  return releaseChannel;
}

export function formatBaseUrl(options: FormatBaseUrlOptions = {}): string {
  const {releaseChannel, suffix} = options;
  let {baseUrl = defaultBaseUrl} = options;
  if (releaseChannel === undefined) {
    baseUrl += `@${getReleaseChannel()}`;
  } else {
    baseUrl += `@${releaseChannel}`;
  }
  baseUrl += '/dist/';
  if (suffix !== undefined) {
    baseUrl += suffix;
  }
  if (baseUrl.slice(-1) !== '/') {
    baseUrl += '/';
  }
  return baseUrl;
}

interface UserscriptPluginOptions {
  headers?: HeadersProps;
  baseUrlOptions?: FormatBaseUrlOptions;
}

const defaultUserscriptHeaders: HeadersProps = {
  name: 'Anon.fm audioplayer userscript',
  license: 'AGPL-3.0-or-later',
  match: ['*://anon.fm/', '*://anon.fm/menu.html'],
  'run-at': RunAt.DocumentEnd,
};

export const userscriptPlugin = (options: UserscriptPluginOptions): WebpackPluginInstance => {
  const {headers, baseUrlOptions} = options;
  const overrideHeaders = {...defaultUserscriptHeaders, ...headers};
  return new WebpackUserscript({
    headers: (headers) => ({...headers, ...overrideHeaders}),
    downloadBaseURL: formatBaseUrl(baseUrlOptions),
    pretty: true,
  });
};

export const commonUserscriptConfig: Configuration = {
  entry: './src/index.ts',
  output: {
    filename: 'audioplayer.js',
    libraryTarget: 'umd',
  },
  mode: 'production',
  module: {
    rules: [imageRulesInline, svgRulesInline, fontRulesInline, tsRules],
  },
  plugins: [webpackDefinePlugin],
  optimization: optimization(),
  stats,
};
