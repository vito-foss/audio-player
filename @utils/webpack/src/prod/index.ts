import {config as deploy} from './deploy';
import {config as userscript} from './userscript';
import {config as userscriptCDN} from './userscript-cdn';

export const prodConfigs = [deploy, userscript, userscriptCDN];
