/*
 * This file is part of AudioPlayer (https://gitlab.com/vitoyucepi/audio-player)
 * Copyright (C) 2023 Vitoyucepi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import path from 'path';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import {
  cleanWebpackPlugin,
  compressionPlugins,
  cssLoaders,
  fontRulesFile,
  imageRulesFile,
  miniCssExtractPlugin,
  optimization,
  optimizationSplit,
  stats,
  svgRulesFile,
  tsRules,
  webpackDefinePlugin,
} from './common';
import {Configuration} from 'webpack';

const context = path.resolve('../../@npm/audioplayer-deploy/');
const htmlWebpackPlugin = new HtmlWebpackPlugin({template: path.resolve(context, './index.html')});

const cssRules = cssLoaders({useFontCdnSource: false}).cssRulesFile;

export const config: Configuration = {
  name: 'Deployment configuration',
  context,
  entry: './src/index.ts',
  output: {
    filename: '[name].[contenthash].js',
    path: path.resolve(context, 'dist'),
  },
  mode: 'production',
  module: {
    rules: [cssRules, imageRulesFile, svgRulesFile, fontRulesFile, tsRules],
  },
  plugins: [
    ...compressionPlugins(),
    miniCssExtractPlugin(),
    htmlWebpackPlugin,
    cleanWebpackPlugin(),
    webpackDefinePlugin,
  ],
  optimization: {...optimization(), ...optimizationSplit},
  stats,
};
