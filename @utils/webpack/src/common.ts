/*
 * This file is part of AudioPlayer (https://gitlab.com/vitoyucepi/audio-player)
 * Copyright (C) 2023 Vitoyucepi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import {Configuration, RuleSetCondition} from 'webpack';
import {resolve} from 'path';

export const cssRulesTest: RuleSetCondition = /\.(?:c|sa|sc)ss$/i;
export const fontRulesTest: RuleSetCondition = /\.(?:woff|woff2|eot|ttf|otf)$/i;
export const tsRulesTest: RuleSetCondition = /\.tsx?$/i;

export const commonConfig: Configuration = {
  output: {
    publicPath: '/',
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.jsx'],
    alias: {
      react: 'preact/compat',
      'react-dom/test-utils': 'preact/test-utils',
      'react-dom': 'preact/compat',
    },
  },
  resolveLoader: {modules: ['node_modules', resolve('node_modules')]},
};
