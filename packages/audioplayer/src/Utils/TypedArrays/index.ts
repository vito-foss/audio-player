/*
 * This file is part of AudioPlayer (https://gitlab.com/vitoyucepi/audio-player)
 * Copyright (C) 2023 Vitoyucepi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

function concatUint8Arrays(typedArrays: Uint8Array[], maxLength = 0): Uint8Array {
  const targetLength = typedArrays.reduce((length, array) => length + array.length, 0);
  const resultArray = new Uint8Array(targetLength);
  typedArrays.reduce((previousLen, buffer) => {
    resultArray.set(buffer, previousLen);
    return previousLen + buffer.length;
  }, 0);
  return resultArray.slice(0, maxLength);
}

function findSequence(haystack: Uint8Array, needle: Uint8Array, offset = 0): number {
  let isBlockDone = false;
  let isSearchFinished = false;
  let newOffset = offset;
  while (!isBlockDone && !isSearchFinished) {
    newOffset = haystack.indexOf(needle[0], newOffset);
    if (newOffset === -1) {
      isBlockDone = true;
    } else if (matchSequence(haystack.slice(newOffset, newOffset + needle.length), needle)) {
      isSearchFinished = true;
    } else {
      newOffset++;
    }
  }
  return newOffset;
}

function matchSequence(firstSequence: Uint8Array, secondSequence: Uint8Array): boolean {
  const length = firstSequence.length;
  if (length !== secondSequence.length) {
    return false;
  }
  let checkedLength = 0;
  for (let position = 0; position < length; position++) {
    if (firstSequence[position] === secondSequence[position]) {
      checkedLength++;
    }
  }
  return checkedLength === length;
}

function stringToUInt8Array(string: string): Uint8Array {
  const uintArray = new Uint8Array(string.length);
  for (let i = 0, inputLength = string.length; i < inputLength; i++) {
    uintArray[i] = string.charCodeAt(i);
  }
  return uintArray;
}

export {findSequence, concatUint8Arrays, stringToUInt8Array};
