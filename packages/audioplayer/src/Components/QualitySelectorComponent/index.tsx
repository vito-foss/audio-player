/*
 * This file is part of AudioPlayer (https://gitlab.com/vitoyucepi/audio-player)
 * Copyright (C) 2023 Vitoyucepi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import {FunctionComponent, h} from 'preact';
import './Styles/index.scss';
import {useStoreon} from 'storeon/preact';
import {useCallback} from 'preact/hooks';
import {QualityEvents, QualityState} from '../../Player';

export const QualitySelectorComponent: FunctionComponent = () => {
  const {dispatch, audioSources, currentSource} = useStoreon<QualityState, QualityEvents>(
    'audioSources',
    'currentSource',
  );
  const onQualityChange = useCallback(
    (event: Event) => {
      const select = event.target as HTMLSelectElement;
      dispatch('quality/set', select.value);
    },
    [dispatch],
  );
  const options = audioSources.map((source) => (
    <option class="quality-selector-option" key={source.name} value={source.name}>
      {source.name}
    </option>
  ));
  return (
    <select class="player-quality-selector" value={currentSource.name} onChange={onQualityChange}>
      {options}
    </select>
  );
};
