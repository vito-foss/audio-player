/*
 * This file is part of AudioPlayer (https://gitlab.com/vitoyucepi/audio-player)
 * Copyright (C) 2023 Vitoyucepi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import {FunctionComponent, h, JSX} from 'preact';
import './Styles/index.scss';
import {MdVolumeDown, MdVolumeMute, MdVolumeOff, MdVolumeUp} from 'react-icons/md';
import {useStoreon} from 'storeon/preact';
import {useCallback} from 'preact/hooks';
import {VolumeEvents, VolumeState} from '../../Player';

export const VolumeButtonComponent: FunctionComponent = () => {
  const {volume, dispatch} = useStoreon<VolumeState, VolumeEvents>('volume');

  const onMouseScroll = useCallback(
    (event: WheelEvent) => {
      event.preventDefault();
      if (event.deltaY < 0) {
        dispatch('volume/set', volume + 5);
      } else if (event.deltaY > 0) {
        dispatch('volume/set', volume - 5);
      }
    },
    [dispatch, volume],
  );

  const onVolumeChange = useCallback(
    ({currentTarget}: JSX.TargetedEvent<HTMLInputElement, Event>) => {
      const value = currentTarget.value;
      dispatch('volume/set', Number(value));
    },
    [dispatch],
  );

  let volumeIcon = <MdVolumeOff />;
  if (volume > VOLUME_STEP_HIGH) {
    volumeIcon = <MdVolumeUp />;
  } else if (volume > VOLUME_STEP_MEDIUM) {
    volumeIcon = <MdVolumeDown />;
  } else if (volume > 0) {
    volumeIcon = <MdVolumeMute />;
  }

  return (
    <div class="player-volume-dropdown" onWheel={onMouseScroll}>
      <button class="player-volume-button">{volumeIcon}</button>
      <div class="player-volume-dropdown-content">
        <div class="player-volume-input-icon">
          <MdVolumeMute size="75%" />
        </div>
        <input
          class="player-volume-input-range"
          type="range"
          min="0"
          max="100"
          value={volume}
          onInput={onVolumeChange}
          onChange={onVolumeChange}
        />
        <div class="player-volume-input-icon">
          <MdVolumeUp size="75%" />
        </div>
      </div>
    </div>
  );
};

const VOLUME_STEP_MEDIUM = 33;
const VOLUME_STEP_HIGH = 66;
