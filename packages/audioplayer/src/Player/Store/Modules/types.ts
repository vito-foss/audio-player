/*
 * This file is part of AudioPlayer (https://gitlab.com/vitoyucepi/audio-player)
 * Copyright (C) 2023 Vitoyucepi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import {GenericTags} from '../../Metadata';
import {AudioSource} from '../../Consts';

type CrossTabFilter = (event: string | number | symbol, data?: unknown) => boolean;

interface VolumeState {
  volume: number;
}

interface VolumeEvents {
  'volume/set': number;
}

type VolumePersist = Array<keyof VolumeState>;

interface PlayState {
  playStatus: 'play' | 'pause' | 'stop';
}

interface PlayEvents {
  'play/set': PlayerState['playStatus'];
}

interface MetadataState {
  tags: GenericTags;
}

interface MetadataEvents {
  'metadata/set': GenericTags;
}

interface QualityState {
  audioSources: AudioSource[];
  currentSource: AudioSource;
}

interface QualityEvents {
  'quality/set': string;
}

type QualityPersist = Array<keyof QualityState>;

type PlayerState = QualityState & PlayState & VolumeState & MetadataState;
type PlayerEvents = QualityEvents & PlayEvents & VolumeEvents & MetadataEvents;

export {
  VolumeEvents,
  VolumeState,
  VolumePersist,
  QualityEvents,
  QualityPersist,
  QualityState,
  PlayEvents,
  PlayState,
  MetadataEvents,
  MetadataState,
  AudioSource,
  CrossTabFilter,
  PlayerState,
  PlayerEvents,
};
