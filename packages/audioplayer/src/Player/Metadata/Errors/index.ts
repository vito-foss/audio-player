/*
 * This file is part of AudioPlayer (https://gitlab.com/vitoyucepi/audio-player)
 * Copyright (C) 2023 Vitoyucepi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

interface UnderflowStreamErrorParams {
  bytesReceived: number;
  bytesExpected: number;
}

class MetadataError extends Error {
  // noinspection LocalVariableNamingConventionJS, JSUnusedGlobalSymbols
  protected __brand: undefined;

  constructor(props = '') {
    super(props);
    this.name = 'MetadataError';
  }
}

class FailedToLoadMetadataError extends MetadataError {
  constructor() {
    super('Metadata download failed.');
    this.name = 'FailedToLoadMetadataError';
  }
}

class UnderflowStreamError extends MetadataError {
  constructor(params: UnderflowStreamErrorParams) {
    super(`Stream ended before enough bytes received. ${params.bytesReceived} of ${params.bytesExpected}`);
    this.name = 'UnderflowStreamError';
  }
}

class NoMetaHeadersError extends MetadataError {
  constructor(props = '') {
    super(`Meta headers don't present in stream. ${props}`.trim());
    this.name = 'NoMetaHeadersError';
  }
}

class FailedToSplitBlock extends MetadataError {
  constructor(props = '') {
    super(`Failed to split block. ${props}`.trim());
    this.name = 'FailedToSplitBlock';
  }
}

export {FailedToSplitBlock, FailedToLoadMetadataError, MetadataError, NoMetaHeadersError, UnderflowStreamError};
