/*
 * This file is part of AudioPlayer (https://gitlab.com/vitoyucepi/audio-player)
 * Copyright (C) 2023 Vitoyucepi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import shallowEqual from 'shallowequal';
import {StoreonStore} from 'storeon';
import {logError} from '../../Utils';
import {MetadataEvents, MetadataState} from '../Store';
import {MP3_SOURCE, STATE_TXT_SOURCE} from '../Consts';
import {GenericTags} from './types';
import {Extractor, Mp3Extractor, StateTxtExtractor} from './Extractors';

class Metadata {
  private static readonly DATA_FETCH_INTERVAL = 10 * 1000;
  private stateTxt: Extractor = new StateTxtExtractor(STATE_TXT_SOURCE);
  private mp3: Extractor = new Mp3Extractor(MP3_SOURCE);
  private isInitialized = false;

  private static fixTags(tags: GenericTags): GenericTags {
    const brokenSeparator = /^ ?-|- ?$/g;
    tags.artist = tags.artist?.replaceAll(brokenSeparator, '').trim();
    tags.name = tags.name?.replaceAll(brokenSeparator, '').trim();
    tags.title = tags.title?.replaceAll(brokenSeparator, '').trim();
    return tags;
  }

  async init(store: StoreonStore<MetadataState, MetadataEvents>): Promise<void> {
    if (this.isInitialized) {
      return;
    }
    this.isInitialized = true;
    store.dispatch('metadata/set', await this.getTags());
    setInterval(() => {
      this.getTags()
        .then((tags) => {
          if (!shallowEqual(store.get().tags, tags)) {
            store.dispatch('metadata/set', tags);
          }
        })
        .catch(function () {
          // do nothing.
        });
    }, Metadata.DATA_FETCH_INTERVAL);
  }

  private async getTags(): Promise<GenericTags> {
    const dataSource = this.dataSourceGenerator();
    let dataSourceInstance = dataSource.next();
    let tags: GenericTags = {};
    while (!dataSourceInstance.done) {
      // eslint-disable-next-line no-await-in-loop
      const getTagsResult = await dataSourceInstance.value.getTags();
      if (getTagsResult.isOk()) {
        tags = Metadata.fixTags({...getTagsResult.value, ...tags});
        if (tags.name || tags.title) {
          return tags;
        }
      } else {
        logError(getTagsResult.error);
      }
      dataSourceInstance = dataSource.next();
    }
    return tags;
  }

  private *dataSourceGenerator(): IterableIterator<Extractor> {
    yield this.stateTxt;
    yield this.mp3;
  }
}

export {Metadata};
