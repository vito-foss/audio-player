/*
 * This file is part of AudioPlayer (https://gitlab.com/vitoyucepi/audio-player)
 * Copyright (C) 2023 Vitoyucepi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import {err, ok} from 'neverthrow';
import {customFetch, logError} from '../../../Utils';
import {GenericTags} from '../types';
import {StateTxtParser} from '../Parsers';
import {FailedToLoadMetadataError} from '../Errors';
import {Extractor, TagsResult} from './types';

class StateTxtExtractor implements Extractor {
  readonly source: RequestInfo;

  constructor(source: RequestInfo) {
    this.source = source;
  }

  async getTags(): Promise<TagsResult> {
    const fetchResponse = await customFetch(this.source);
    if (fetchResponse.isErr()) {
      logError(fetchResponse.error);
      return err(new FailedToLoadMetadataError());
    }
    const responseText = await fetchResponse.value.text();
    const parsedTags = StateTxtParser.parseText(responseText);
    if (parsedTags.isErr()) {
      return err(parsedTags.error);
    }
    return ok(this.extractGenericTags(parsedTags.value));
  }

  extractGenericTags(tags: Record<string, string>): GenericTags {
    const {
      TITLE: title,
      ARTIST: artist,
      ALBUMARTIST: albumArtist,
      COMPOSER: composer,
      NAME: name,
      ISLIVE: isLive,
    } = tags;
    const genericTags: GenericTags = {};
    switch (isLive) {
      case '0':
        genericTags.isLive = false;
        break;
      case '1':
        genericTags.isLive = true;
        break;
    }
    genericTags.artist = artist || albumArtist || composer;
    if (title) {
      if (genericTags.artist) {
        genericTags.title = title;
      } else {
        const separatorPosition = title.indexOf(' - ');
        if (separatorPosition > -1 && title.lastIndexOf(' - ')) {
          genericTags.artist = title.slice(0, separatorPosition);
          genericTags.title = title.slice(separatorPosition + 3);
        } else {
          genericTags.title = title;
        }
      }
    }
    if (name) {
      genericTags.name = name;
    }
    return genericTags;
  }
}

export {StateTxtExtractor};
