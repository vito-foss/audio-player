/*
 * This file is part of AudioPlayer (https://gitlab.com/vitoyucepi/audio-player)
 * Copyright (C) 2023 Vitoyucepi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import {concatUint8Arrays, customFetch, logError} from '../../../Utils';
import {err, ok, Result} from 'neverthrow';
import {FailedToLoadMetadataError, MetadataError, NoMetaHeadersError, UnderflowStreamError} from '../Errors';
import {GenericTags} from '../types';
import {IcyParser, IcyParserSplitResult} from '../Parsers';
import {Extractor, TagsResult} from './types';

class Mp3Extractor implements Extractor {
  // noinspection MagicNumberJS
  private static readonly HEADER_SIZE = 16 * 2 ** 10;
  private static readonly FETCH_HEADERS = {'Icy-Metadata': '1'};
  readonly source: RequestInfo;

  constructor(source: RequestInfo) {
    this.source = source;
  }

  private static async fetchBlock(
    reader: ReadableStreamDefaultReader<Uint8Array>,
  ): Promise<Result<Uint8Array, MetadataError>> {
    let bytesReceived = 0;
    let isStreamDone = false;
    const responseBlocksArray = [];
    while (bytesReceived < Mp3Extractor.HEADER_SIZE && !isStreamDone) {
      // eslint-disable-next-line no-await-in-loop
      const responseBlock = await reader.read();
      isStreamDone = responseBlock.done;
      const responseBlockValue = responseBlock.value;
      if (responseBlockValue === undefined) {
        return err(new UnderflowStreamError({bytesReceived, bytesExpected: Mp3Extractor.HEADER_SIZE}));
      }
      bytesReceived += responseBlockValue.length;
      responseBlocksArray.push(responseBlockValue);
    }
    reader.cancel().catch(function () {
      // do nothing.
    });
    if (bytesReceived < Mp3Extractor.HEADER_SIZE) {
      return err(new UnderflowStreamError({bytesReceived, bytesExpected: Mp3Extractor.HEADER_SIZE}));
    }
    const uint8ResponseArray = concatUint8Arrays(responseBlocksArray, Mp3Extractor.HEADER_SIZE);
    return ok(uint8ResponseArray);
  }

  async getTags(): Promise<TagsResult> {
    const fetchResponse = await customFetch(this.source, {headers: Mp3Extractor.FETCH_HEADERS});
    if (fetchResponse.isErr()) {
      logError(fetchResponse.error);
      return err(new FailedToLoadMetadataError());
    }
    if (fetchResponse.value.body === null) {
      return err(new FailedToLoadMetadataError());
    }
    const fetchReader = fetchResponse.value.body.getReader();
    const block = await Mp3Extractor.fetchBlock(fetchReader);
    if (block.isErr()) {
      return err(block.error);
    }
    let httpHeaders: Result<Record<string, string>, Error>;
    let splitResult: Result<IcyParserSplitResult, MetadataError>;
    if (fetchResponse.value.headers.has('icy-metaint')) {
      httpHeaders = IcyParser.parseHttpHeaders(fetchResponse.value.headers);
      splitResult = ok({headerBlock: new Uint8Array(0), bodyBlock: block.value});
    } else {
      splitResult = IcyParser.split(block.value);
      if (splitResult.isErr()) {
        return err(splitResult.error);
      }
      httpHeaders = IcyParser.parseHeader(splitResult.value.headerBlock);
    }
    if (httpHeaders.isErr()) {
      logError(httpHeaders.error);
      return err(new FailedToLoadMetadataError());
    }
    if (!httpHeaders.value['ICY-METAINT']) {
      return err(new NoMetaHeadersError());
    }
    const metaHeaders = IcyParser.parseBody(
      splitResult.value.bodyBlock,
      parseInt(httpHeaders.value['ICY-METAINT'], 10),
    );
    if (metaHeaders.isErr()) {
      return err(metaHeaders.error);
    }
    return ok(this.extractGenericTags({...httpHeaders.value, ...metaHeaders.value}));
  }

  extractGenericTags(tags: Record<string, string>): GenericTags {
    const {'ICY-NAME': name, STREAMTITLE: title} = tags;
    const genericTags: GenericTags = {};
    if (name) {
      genericTags.name = name;
    }
    if (title) {
      const separatorPosition = title.indexOf(' - ');
      if (separatorPosition > -1 && separatorPosition === title.lastIndexOf(' - ')) {
        genericTags.artist = title.slice(0, separatorPosition);
        genericTags.title = title.slice(separatorPosition + 3);
      } else {
        genericTags.title = title;
      }
    }
    return genericTags;
  }
}

export {Mp3Extractor};
