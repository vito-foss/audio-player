/*
 * This file is part of AudioPlayer (https://gitlab.com/vitoyucepi/audio-player)
 * Copyright (C) 2023 Vitoyucepi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

class IndexPage {
  private static addFrames() {
    const framesContainer = document.createElement('div');
    framesContainer.className = 'frames-container';
    const menuFrame = document.createElement('iframe');
    menuFrame.className = 'menu-frame content-frame';
    menuFrame.name = 'menu';
    menuFrame.src = 'https://anon.fm/menu.html';
    framesContainer.appendChild(menuFrame);
    const infoFrame = document.createElement('iframe');
    infoFrame.className = 'info-frame content-frame';
    infoFrame.src = 'https://anon.fm/info.html';
    infoFrame.name = 'CONTENT';
    framesContainer.appendChild(infoFrame);
    document.body.appendChild(framesContainer);
  }

  private static preparePage() {
    document.body.remove();
    document.body = document.createElement('body');
  }

  private static addScript() {
    // eslint-disable-next-line @typescript-eslint/no-require-imports
    require('@vitoyucepi/audioplayer');
  }

  private static addStyle() {
    // eslint-disable-next-line @typescript-eslint/no-require-imports
    require('./index.scss');
  }

  init() {
    IndexPage.preparePage();
    IndexPage.addStyle();
    IndexPage.addScript();
    IndexPage.addFrames();
  }
}

const indexPage = new IndexPage();
indexPage.init();
